package com.rnm.test.assignment.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mahe on 9/26/2017.
 */

public class Lessons {

    @SerializedName("lesson_data")
    @Expose
    private List<LessonData> lessonData = null;

    public List<LessonData> getLessonData() {
        return lessonData;
    }

    public void setLessonData(List<LessonData> lessonData) {
        this.lessonData = lessonData;
    }
}
