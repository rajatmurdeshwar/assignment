package com.rnm.test.assignment.postman;

import com.rnm.test.assignment.models.LessonData;
import com.rnm.test.assignment.models.Lessons;


import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Mahe on 9/26/2017.
 */

public interface Services {
    @GET(ServiceConstant.URL)
    public Call<Lessons> getLessonsData();


}
