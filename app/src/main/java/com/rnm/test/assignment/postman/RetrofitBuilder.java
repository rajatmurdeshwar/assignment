package com.rnm.test.assignment.postman;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mahe on 9/26/2017.
 */

public class RetrofitBuilder {
    private static Retrofit obj = null;
    public static final String BASE_URL ="http://www.akshaycrt2k.com/";
    public static Retrofit getRetrofit()
    {
        if(obj==null)
        {
            obj= new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        }
        return obj;
    }

}
