package com.rnm.test.assignment.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mahe on 9/26/2017.
 */

public class LessonData {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("conceptName")
    @Expose
    private String conceptName;
    @SerializedName("pronunciation")
    @Expose
    private String pronunciation;
    @SerializedName("targetScript")
    @Expose
    private String targetScript;
    @SerializedName("audio_url")
    @Expose
    private String audioUrl;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getConceptName() {
        return conceptName;
    }

    public void setConceptName(String conceptName) {
        this.conceptName = conceptName;
    }

    public String getPronunciation() {
        return pronunciation;
    }

    public void setPronunciation(String pronunciation) {
        this.pronunciation = pronunciation;
    }

    public String getTargetScript() {
        return targetScript;
    }

    public void setTargetScript(String targetScript) {
        this.targetScript = targetScript;
    }

    public String getAudioUrl() {
        return audioUrl;
    }

    public void setAudioUrl(String audioUrl) {
        this.audioUrl = audioUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LessonData that = (LessonData) o;

        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (conceptName != null ? !conceptName.equals(that.conceptName) : that.conceptName != null)
            return false;
        if (pronunciation != null ? !pronunciation.equals(that.pronunciation) : that.pronunciation != null)
            return false;
        if (targetScript != null ? !targetScript.equals(that.targetScript) : that.targetScript != null)
            return false;
        return audioUrl != null ? audioUrl.equals(that.audioUrl) : that.audioUrl == null;

    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (conceptName != null ? conceptName.hashCode() : 0);
        result = 31 * result + (pronunciation != null ? pronunciation.hashCode() : 0);
        result = 31 * result + (targetScript != null ? targetScript.hashCode() : 0);
        result = 31 * result + (audioUrl != null ? audioUrl.hashCode() : 0);
        return result;
    }

}
