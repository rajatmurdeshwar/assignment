package com.rnm.test.assignment;


import android.app.Service;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.rnm.test.assignment.fragments.LessonFragment;
import com.rnm.test.assignment.fragments.QuestionFragment;
import com.rnm.test.assignment.models.LessonData;
import com.rnm.test.assignment.models.Lessons;
import com.rnm.test.assignment.postman.RetrofitBuilder;
import com.rnm.test.assignment.postman.Services;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    Button learnData;
    Button questionData;
    Services apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        learnData = (Button) findViewById(R.id.lesson_btn);
        questionData = (Button) findViewById(R.id.question_btn);
    }

    public void postman() {
        Retrofit retrofit = RetrofitBuilder.getRetrofit();
        apiInterface = retrofit.create(Services.class);
        Call<Lessons> call = apiInterface.getLessonsData();
        call.enqueue(new Callback<Lessons>() {
            @Override
            public void onResponse(Call<Lessons> call, Response<Lessons> response) {
                Log.d(TAG, String.valueOf(response));
                if(response.isSuccessful()){
                    Lessons data = response.body();
                    data.getLessonData();
                }
            }

            @Override
            public void onFailure(Call<Lessons> call, Throwable t) {
                Log.e(TAG,t.toString());

            }
        });



    }


    public void learnData(View view) {
        postman();
        LessonFragment lf = new LessonFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.lessonFragment1,lf).commit();




    }

    public void questionData(View view) {
        postman();
        QuestionFragment lf = new QuestionFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.lessonFragment1,lf).commit();
    }
}
